from django.db import models

class Note(models.Model):
    to = models.CharField(max_length = 30)
    from_user = models.CharField(max_length = 30)
    title = models.CharField(max_length = 30)
    message = models.TextField()