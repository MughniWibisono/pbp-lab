import 'package:flutter/material.dart';
import './myhomepage.dart';

class About extends StatelessWidget {
    const About({Key? key}) : super(key : key,);
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: const Text("BansosPeduli",
                            style: TextStyle(
                                    fontWeight: FontWeight.bold
                            ),
                    ),
                backgroundColor: Colors.blueAccent[400],
            ),

            body: Column(
                children: <Widget> [
                    Container (
						margin: const EdgeInsets.fromLTRB(0,100,0,20),
                        child: const Text("About Us",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 50,
                            ),
						textAlign: TextAlign.center,
                        ),
                    ),

                    Container(
						margin: const EdgeInsets.fromLTRB(10,0,0,0),
                        child: const Text("Hi there! \nHere at Bansos Peduli we help each other by giving people that got positive for covid free stuffs. Stuffs like foods, clothing, soap, dental kit, etc",
                            style: TextStyle(
                                fontSize: 25,
                            ),
						textAlign: TextAlign.center,
                        ),
                    ),
                ]
            ),

            endDrawer: Drawer(
                child: ListView(
                    children: <Widget> [
						SizedBox (
							height: 64,
							child: DrawerHeader(
								decoration: BoxDecoration(
									color: Colors.blueAccent[400],
								),

								child: const Text('BansosPeduli',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                        fontStyle: FontStyle.italic,
                                        fontSize: 25,
                                    )
                                ),
							),
						),

                        // for item 1
                        ListTile(
                            title: const Text('Home',
								style: TextStyle(
									fontWeight: FontWeight.bold,
									),
								),
                                onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => const MyHomePage()),
                                    );
                                },
                        ),
                    ],
                ),
            ),
        );
    }
}