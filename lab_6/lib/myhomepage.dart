import 'package:flutter/material.dart';
import './about.dart';
import './register.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyHomePage extends StatelessWidget {
    const MyHomePage({Key? key}) : super(key : key,);
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: const Text("BansosPeduli",
                            style: TextStyle(
                                fontWeight: FontWeight.bold
                            ),
                        ),
                backgroundColor: Colors.blueAccent[400],
            ),

            body: Column(
				children: <Widget> [
					Container(
                        margin: const EdgeInsets.fromLTRB(20,50,30,10),
                        child: const Text("Segera daftar dan berpartisipasi dalam program Bansos COVID-19",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 40,
                                ),
                        ),
                    ),

                    SizedBox(
                        width: 150,
                        height: 40,
                        child: ElevatedButton(
                            onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => const Register()),
                                );
                            },
                            child: const Text('Register',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize:20,
                                ),),
                        ),
                    ),

					const SizedBox(height: 60),

					Container(
						margin: EdgeInsets.only(bottom: 10),
						child: const Text("DATA COVID-19 INDONESIA",
							style: TextStyle(
								fontWeight: FontWeight.bold,
								fontSize: 25,
								color: Colors.redAccent,
							),
							textAlign: TextAlign.center,
						),
					),

					Row (
						mainAxisAlignment: MainAxisAlignment.center,
						children: <Widget> [
							Column(
								children: [
									Column(
										children: <Widget> [
											IconButton(
												alignment: Alignment.center,
												icon: const FaIcon(FontAwesomeIcons.plusCircle), 
												onPressed: () {},
											),

											Container(
												margin: const EdgeInsets.fromLTRB(20, 0, 30, 0),
												child: const Text("POSITIF",
													style: TextStyle(
														fontWeight: FontWeight.bold,
														fontSize: 16,
													),
												),
											),
											
											Container(
												margin: const EdgeInsets.fromLTRB(20, 0, 30, 30),
												child: const Text("4,251,945",
													style: TextStyle(
														fontWeight: FontWeight.bold,
														color: Colors.blueAccent,
													),
												),
											),
										]
									),

									Column(
										children: <Widget> [
											IconButton(
												alignment: Alignment.center,
												icon: const FaIcon(FontAwesomeIcons.solidHeart), 
												onPressed: () {},
											),

											Container(
												margin: const EdgeInsets.fromLTRB(20, 0, 30, 0),
												child: const Text("SEUMBUH",
													style: TextStyle(
														fontWeight: FontWeight.bold,
														fontSize: 16,
													)),
											),
											
											Container(
												margin: const EdgeInsets.fromLTRB(20, 0, 30, 10),
												child: const Text("4,100,321",
													style: TextStyle(
														fontWeight: FontWeight.bold,
														color: Colors.blueAccent,
													),
												),
											),
										]
									),
								],
							),

							Column (
								children: <Widget> [
									Column(
										children: <Widget> [
											IconButton(
												alignment: Alignment.center,
												icon: const FaIcon(FontAwesomeIcons.procedures), 
												onPressed: () {},
											),
											
											Container(
												margin: const EdgeInsets.fromLTRB(30, 0, 20, 0),
												child: const Text("DIRAWAT",
													style: TextStyle(
														fontWeight: FontWeight.bold,
														fontSize: 16,
													)),
											),
											
											Container(
												margin: const EdgeInsets.fromLTRB(30, 0, 20, 30),
												child: const Text("8,315",
													style: TextStyle(
														fontWeight: FontWeight.bold,
														color: Colors.blueAccent,
													),
												),
											),
										]
									),

									Column(
										children: <Widget> [
											IconButton(
												alignment: Alignment.center,
												icon: const FaIcon(FontAwesomeIcons.skull), 
												onPressed: () {},
											),

											Container(
												margin: const EdgeInsets.fromLTRB(30, 0, 20, 0),
												child: const Text("MENINGGAL",
													style: TextStyle(
														fontWeight: FontWeight.bold,
														fontSize: 16,
													)),
											),
											
											Container(
												margin: const EdgeInsets.fromLTRB(30, 0, 20, 10),
												child: const Text("143,709",
													style: TextStyle(
														fontWeight: FontWeight.bold,
														color: Colors.blueAccent,
													),
												),
											),
										]
									),
								],
							),
						],
					),
				],
			),

            endDrawer: Drawer(
                child: ListView(
                    children: <Widget> [
						SizedBox (
							height: 64,
							child: DrawerHeader(
								decoration: BoxDecoration(
									color: Colors.blueAccent[400],
								),

								child: const Text('BansosPeduli',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                        fontStyle: FontStyle.italic,
                                        fontSize: 25,
                                    )
                                ),
							),
						),

                        // for item 1
                        ListTile(
                            title: const Text('About',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    ),
                                ),
                                onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => const About()),
                                    );
                                },
                        ),
                    ],
                ),
            ),
        );
    }
}