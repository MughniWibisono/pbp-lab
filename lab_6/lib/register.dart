import 'package:flutter/material.dart';
import './myhomepage.dart';
import './about.dart';
import './submited.dart';

class Register extends StatelessWidget {
    const Register({Key? key}) : super(key : key,);
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: const Text("BansosPeduli",
                            style: TextStyle(
                                    fontWeight: FontWeight.bold
                            ),
                    ),
                backgroundColor: Colors.blueAccent[400],
            ),

            body: Column(
                children: <Widget> [
					Container (
						decoration: BoxDecoration(
							color: Colors.blueAccent[400],
							borderRadius: BorderRadius.circular(5),
						),

						margin: EdgeInsets.only(top: 100),
						padding: const EdgeInsets.fromLTRB(68,18,68,18),
						child: const Text("Register now!",
							style: TextStyle(
								fontWeight: FontWeight.bold,
								fontSize: 25,
								color: Colors.white,
							),
						),
					),

					const SizedBox(height: 10),
					
					const Padding(
						padding: EdgeInsets.fromLTRB(50,0,50,0),
						child: TextField(
							decoration: InputDecoration(
							border: OutlineInputBorder(),
							hintText: 'Username',
						),
						),
					),

					const SizedBox(height: 10),

					const Padding(
						padding: EdgeInsets.fromLTRB(50,0,50,0),
						child: TextField(
							decoration: InputDecoration(
							border: OutlineInputBorder(),
							hintText: 'Password',
						),
						),
					),

					const SizedBox(height: 15),

					SizedBox(
						width: 100,
						height: 40,
						child: ElevatedButton(
							onPressed: () {
								Navigator.push(
									context,
									MaterialPageRoute(builder: (context) => const Submited()),
								);
							},
							child: const Text("Submit",
								style: TextStyle(
									fontWeight: FontWeight.bold,
								))
						)
					)
                ]
            ),

            endDrawer: Drawer(
                child: ListView(
                    children: <Widget> [
						SizedBox (
							height: 64,
							child: DrawerHeader(
								decoration: BoxDecoration(
									color: Colors.blueAccent[400],
								),

								child: const Text('BansosPeduli',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                        fontStyle: FontStyle.italic,
                                        fontSize: 25,
                                    )
                                ),
							),
						),

                        // for item 1
                        ListTile(
                            title: const Text('Home',
								style: TextStyle(
									fontWeight: FontWeight.bold,
									),
								),
                                onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => const MyHomePage()),
                                    );
                                },
                        ),

                        ListTile(
                            title: const Text('About',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    ),
                                ),
                                onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => const About()),
                                    );
                                },
                        ),
                    ],
                ),
            ),
        );
    }
}