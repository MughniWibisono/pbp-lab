from django.shortcuts import render
from lab_2.models import Note

# Create your views here.
def index(request):
    response = {"data":Note.objects.all()}
    return render(request, 'lab5_index.html', response)

def get_note(request):
    response = {"data":Note.objects.all()}
    