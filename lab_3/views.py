from lab_1.models import Friend
from .forms import FriendForm
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url="/admin/login/") # there's no significant difference with or without
def index(request):
    friend = Friend.objects.all()
    response = {"friends":friend}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/") # there's no significant difference with or without
def add_friend(request):
    if request.method == 'POST':
        form = FriendForm(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect('/lab-3')

    form = FriendForm()
    context = {"form":form}

    return render(request, "lab3_form.html", context)