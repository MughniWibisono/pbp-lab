1. Difference between JSON and XML:
To begin with, JSON is a data format, while XML is a markup language. XPath allows you to create a query and receive an answer. Metadata, attributes, and namespaces can all be added to XML documents. Furthermore, combining XML with XSL, XSD, XQuery, and other technologies creates a powerful combo. These are some key characteristics that continue to distinguish XML.
(https://hackr.io/blog/json-vs-xml)

2. Difference between HTML and XML:
HTML and XML are closely related, with HTML displaying data and describing the structure of a webpage, and XML storing and transferring data. HTML is a predefined language that defines other languages, whereas XML is a standard language that defines other languages.
(https://www.upgrad.com/blog/html-vs-xml/)